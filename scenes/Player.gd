extends KinematicBody2D

signal hit

export (int) var SPEED = 150
var velocity = Vector2()
var screensize

func _ready():
	screensize = get_viewport_rect().size

func get_input():
	# Detect up/down/left/right keystate and only move when pressed.
	velocity = Vector2()
	if Input.is_action_pressed('ui_right'):
		#print("right")
		velocity.x += 1
		#$AnimatedSprite.animation = "right"
	if Input.is_action_pressed('ui_left'):
		#print("left")
		velocity.x -= 1
		#$AnimatedSprite.animation = "left"
	if Input.is_action_pressed('ui_down'):
		#print("down")
		velocity.y += 1
		#$AnimatedSprite.animation = "down"
	if Input.is_action_pressed('ui_up'):
		#print("up")
		velocity.y -= 1
		#$AnimatedSprite.animation = "up"
	#print("velocity: " + str(velocity))	
	velocity = velocity.normalized() * SPEED
	
	#print("normalised velocity: " + str(velocity))
	if velocity.length() > 0:
		$AnimatedSprite.play()
		velocity = velocity.normalized() * SPEED
	else:
		$AnimatedSprite.stop()
		$AnimatedSprite.animation = "up"
	
	if velocity.x > 0:
		$AnimatedSprite.animation = "right"
	elif velocity.x < 0:
		$AnimatedSprite.animation = "left"
	if velocity.y > 0:
		$AnimatedSprite.animation = "down"
	elif velocity.y < 0:
		$AnimatedSprite.animation = "up"
		

func _physics_process(delta):
	get_input()
	#print("_physics_process velocity: " + str(velocity))
	move_and_collide(velocity * delta)
	

#func _process(delta):
#	velocity = Vector2()
#	if Input.is_action_pressed("ui_right"):
#		velocity.x += 1
#	if Input.is_action_pressed("ui_left"):
#		velocity.x -= 1
#	if Input.is_action_pressed("ui_up"):
#		velocity.y -= 1
#	if Input.is_action_pressed("ui_down"):
#		velocity.y += 1
#	if velocity.length() > 0:
#		$AnimatedSprite.play()
#		velocity = velocity.normalized() * SPEED
#	else:
#		$AnimatedSprite.stop()
#		$AnimatedSprite.animation = "up"
#
#	position += velocity * delta
#	position.x = clamp(position.x, 0, screensize.x)
#	position.y = clamp(position.y, 0, screensize.y)	
#
#	if velocity.x > 0:
#		$AnimatedSprite.animation = "right"
#	elif velocity.x < 0:
#		$AnimatedSprite.animation = "left"
#	if velocity.y > 0:
#		$AnimatedSprite.animation = "down"
#	elif velocity.y < 0:
#		$AnimatedSprite.animation = "up"
	
func _on_Area2D_body_entered(body):
	#emit_signal("hit")
	if body.get_groups().has("chickens"):
		print("fox hit: " + str(body.get_name()))
		body.EatChicken()
