extends RigidBody2D

onready var HUD  = get_parent().get_node("HUD")

export (int) var MIN_SPEED
export (int) var MAX_SPEED

signal add_score

var velocity = Vector2()
var screensize

var chicken_directions = ["up", "down", "left", "right"]

func _ready():
	$AnimatedSprite.animation = chicken_directions[randi()% chicken_directions.size()]
	print("$AnimatedSprite.animation: " + str($AnimatedSprite.animation))
	$AnimatedSprite.play()
	$Tweeny.interpolate_property($AnimatedSprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tweeny.interpolate_property($AnimatedSprite, "scale" , $AnimatedSprite.scale , Vector2(3.0, 3.0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	self.add_to_group("chickens")
	
	#print(HUD.get_name())


func _physics_process(delta):
	velocity = get_linear_velocity()
	
#	if velocity.x > 0:
#		$AnimatedSprite.animation = "right"
#	elif velocity.x < 0:
#		$AnimatedSprite.animation = "left"
#	if velocity.y > 0:
#		$AnimatedSprite.animation = "down"
#	elif velocity.y < 0:
#		$AnimatedSprite.animation = "up"
		
func EatChicken():
	#we are here because this chicken has been eaten
	HUD.AddScore(1)
	$Squawk.play()
	$Tweeny.start()
	

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Timer_timeout():
	$Sound.play()
	

func _on_Tweeny_tween_completed(object, key):
	queue_free()
