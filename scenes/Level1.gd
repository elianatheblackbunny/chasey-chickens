extends Node

export (PackedScene) var Mob

var score

func _ready():
	randomize()
	
func NewGame():
	score = 0
	

func _on_chicken_timer_timeout():
	$MobPath/MobSpawnLocation.set_offset(randi())
	var mob = Mob.instance()
	add_child(mob)
	mob.position = $MobPath/MobSpawnLocation.position
	#mob.position = $ChickenSpawnLocation.position
	mob.set_linear_velocity(Vector2(rand_range(mob.MIN_SPEED, mob.MAX_SPEED), rand_range(mob.MIN_SPEED, mob.MAX_SPEED)))


